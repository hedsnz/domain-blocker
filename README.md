# Domain blocker

Provide a list of domains to be blocked (so that you can't
access specific websites). Works by modifying the /etc/hosts file.
Designed for Linux.

## Requirements

- Python 3

## Usage

1. Clone the repository into your local filesystem
2. Edit sites.txt: Add sites to be blocked (with relevant subdomains)
on new lines. The main repository includes YouTube as an example.
3. In the terminal, in the root domain-blocker directory, run `sudo python3 domain-blocker`.
4. Check that it worked by running `cat /etc/hosts` in the terminal. The
output should include the list of domains in sites.txt.

## References

This is loosely based on ideas found [here](https://www.geeksforgeeks.org/website-blocker-using-python/) and [here](https://www.tutorialspoint.com/website-blocker-using-python) (I have 
no idea which is the primary source). Also [this](https://stackoverflow.com/questions/14340283/how-to-write-to-a-specific-line-in-file-in-python) SO post.