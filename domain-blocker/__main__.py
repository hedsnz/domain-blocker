import hosts

def main():
    
    # Instantiate hosts object
    host = hosts.host()

    # Parse the sites file
    sites = host.parse_sites()

    # Parse the hosts file to check whether the
    # domain-blocker has been used before. If not,
    # then append the header and footer.
    if not host.header in host.parse_hosts():
        host.write_header_and_footer()
    
    # Write the sites to the hosts file
    host.write_sites(sites=sites)

if __name__ == '__main__':
    main()