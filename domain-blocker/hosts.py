
class host:
    def __init__(self):
        self.file = "/etc/hosts"
        self.sites = "sites.txt"
        self.redirect = "0.0.0.0"
        self.header = "# ----- Start entries managed by domain-blocker.py\n"
        self.footer = "# ----- End entries managed by domain-blocker.py"
        self.content = None
        self.update = False

    def parse_sites(self):
        """
        Retrieve sites to block from sites.txt.
        """
        with open(self.sites) as file:
            sites = file.readlines()
            sites = [line.rstrip() for line in sites]
            return sites

    def parse_hosts(self):
        """
        Parse the /etc/hosts file
        """
        with open(self.file, 'r') as file:
            return file.read().splitlines()

    def write_header_and_footer(self):
        with open(self.file, 'a') as file:
            file.write(self.header)
            file.write(self.footer)

    def write_sites(self, sites):
        """
        Write sites to hosts file between the domain-blocker
        header and footer.
        sites: return value of parse_sites.
        https://stackoverflow.com/questions/14340283/how-to-write-to-a-specific-line-in-file-in-python
        """
        with open(self.file, "r") as f:
            lines = f.readlines()
        with open(self.file, "w") as f:
            # We want to overwrite any lines found between the domain-blocker
            # header and footer. That is, we remove any domains that aren't in
            # the current sites.txt file, and we add all the domains that are.
            # To do this (and not affect the rest of the file), we loop over
            # the lines in the input file and track whether they are between
            # the header and footer or not. If they aren't, then write as normal.
            between_header_and_footer = False
            # Also we check sites_written, for
            # some reason I can't figure out, it tries to write the block
            # twice...
            sites_written = False
            for line in lines:
                # Write sites only under the header. Note that we need
                # to only allow when between_header_and_footer is False
                # because otherwise it'll just reproduce the entire previous
                # domain-blocker entry/block. 
                if line == self.header and not between_header_and_footer and not sites_written:
                    between_header_and_footer = True
                    f.write(self.header)
                    for site in sites:
                        f.write(self.redirect + " " + site + "\n")
                    sites_written = True
                if line == self.footer:
                    between_header_and_footer = False
                    f.write(self.footer)
                    f.write("\n")
                    continue
                # Reproduce existing lines that aren't between
                # the header and footer
                if not between_header_and_footer and line != self.header and line != self.footer:
                    f.write(line)